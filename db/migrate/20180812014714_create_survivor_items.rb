class CreateSurvivorItems < ActiveRecord::Migration[5.2]
  def change
    create_table :survivor_items do |t|
      t.references :survivor, foreign_key: true, null:false
      t.references :item, foreign_key: true, null:false
      t.integer :quantity, null:false
      t.boolean :accessible, null:false, default: true

      t.index [:survivor_id, :item_id], unique: true

      t.timestamps
    end
  end
end
