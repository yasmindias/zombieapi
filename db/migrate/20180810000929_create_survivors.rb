class CreateSurvivors < ActiveRecord::Migration[5.2]
  def change
    create_table :survivors do |t|
      t.string :name, null:false
      t.integer :age, null:false
      t.string :gender, null:false
      t.string :last_location, null:false
      t.integer :reported_infection_count, null:false, default: 0

      t.timestamps
    end
  end
end
