# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

50.times do
Survivor.create({
    name: Faker::Name.name,
    age: Faker::Number.between(14, 70),
    gender: Faker::Gender.binary_type,
    last_location: "#{Faker::Address.latitude}, #{Faker::Address.longitude}",
    reported_infection_count: 0
                })
end

Item.create({
    name: "Water",
    points: 4
})

Item.create({
    name: "Food",
    points: 3
})

Item.create({
    name: "Medication",
    points: 2
})

Item.create({
    name: "Ammunition",
    points: 1
})

SurvivorItem.create({
    survivor_id: 10,
    item_id: 1,
    quantity: 4,
    accessible: true
})

SurvivorItem.create({
    survivor_id: 20,
    item_id: 3,
    quantity: 4,
    accessible: false
})

SurvivorItem.create({
    survivor_id: 40,
    item_id: 2,
    quantity: 3,
    accessible: false
})

SurvivorItem.create({
    survivor_id: 25,
    item_id: 4,
    quantity: 4,
    accessible: false
})

SurvivorItem.create({
    survivor_id: 15,
    item_id: 3,
    quantity: 6,
    accessible: false
})

SurvivorItem.create({
    survivor_id: 12,
    item_id: 1,
    quantity: 4,
    accessible: true
})

SurvivorItem.create({
    survivor_id: 32,
    item_id: 2,
    quantity: 4,
    accessible: true
})