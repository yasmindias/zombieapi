class SurvivorsController < ApplicationController
  before_action :set_survivor, only: [:show, :destroy, :update, :report_infection]

  def index
    survivor = Survivor.order('created_at DESC').all
    render json: survivor, status: :ok
  end

  def show
    render json: @survivor, status: :ok
  end

  def create
    survivor = Survivor.new(survivor_params)
    if survivor.save
      survivor.add_items(params[:inventory])
      render json: survivor, status: :ok
    else
      render json: survivor.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @survivor.destroy
    render json: @survivor, status: :ok
  end

  def update
    if @survivor.update(params[:last_location])
      render json: @survivor, status: :ok
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  def report_infection
    @survivor.reported_infection_count = @survivor.reported_infection_count.to_i + 1
    if @survivor.save
      render json: @survivor, status: :ok
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  def trade
    TradeService.new().trade(params)
  end

  def reports
      report_service = ReportService.new()

      report = report_service.survivor_counters
      report["points_lost_due_to_infection"] = report_service.points_lost
      report["average_by_items"] = report_service.average_by_items

      render json: report, status: :ok
  end

  private

  def survivor_params
    params.require([:name, :age, :gender, :last_location, :inventory])
  end

  def set_survivor
    @survivor = Survivor.find(params[:id])
  end
end