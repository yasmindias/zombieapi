class Survivor < ApplicationRecord
  COUNT_INFECTION = 3

  before_save :update_accessibility

  validates :name, :presence => true

  has_many :survivor_items

  def update_accessibility
    make_items_inaccessible if infected?
  end

  def infected?
    reported_infection_count >= COUNT_INFECTION
  end

  def make_items_inaccessible
    survivor_items.update_all(accessible: false)
  end

  def add_items(items)
    items.each do |item|
      SurvivorItem.create(
          {
              survivor_id: id,
              item_id: item[:id],
              quantity: item[:quantity]
          })
    end
  end
end
