class ReportService
    def survivor_counters
        infected_count = Survivor.where("reported_infection_count >= 3").count(:id)
        non_infected_count = Survivor.where("reported_infection_count < 3").count(:id)
        total_survivors = infected_count+non_infected_count

        {pct_infected_survivors: "#{(infected_count*100)/total_survivors}%",
         pct_non_infected_survivors: "#{(non_infected_count*100)/total_survivors}%"}
    end

    def points_lost
        SurvivorItem.left_outer_joins(:item, :survivor).where("reported_infection_count >= 3").sum("points*quantity")
    end

    def average_by_items
        total_survivors = Survivor.where("reported_infection_count < 3").count(:id)

        quantity_by_item = SurvivorItem.left_outer_joins(:item, :survivor).
            group(:item_id).order(:item_id).select("SUM(quantity) AS quantity")
        
        {"Water": (quantity_by_item[0].quantity/total_survivors),
         "Food": (quantity_by_item[1].quantity/total_survivors),
         "Medication": (quantity_by_item[2].quantity/total_survivors),
         "Ammunition": (quantity_by_item[3].quantity/total_survivors)}
    end
end