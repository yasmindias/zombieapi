class TradeService
  def isInfected(survivor_id)
    survivor = Survivor.find(survivor_id)
    survivor.reported_infection_count >= Survivor::COUNT_INFECTION
  end

  def hasSamePoints(giving, receiving)
    giver_points = 0
    giver_quantity = 0
    receiver_points = 0
    receiver_quantity = 0

    giving.each do |item|
      giver_points += Item.find(item[:id]).points
      giver_quantity += item[:quantity]
    end

    receiving.each do |item|
      receiver_points += Item.find(item[:id]).points
      receiver_quantity += item[:quantity]
    end

    total_giver_points = giver_points*giver_quantity
    total_receiver_points = receiver_points*receiver_quantity

    total_giver_points == total_receiver_points
  end

  def trade(params)
    @giver_id = params[:giver_id]
    @receiver_id = params[:receiver_id]

    Item.transaction do
      if !isInfected(@giver_id)
        if !isInfected(@receiver_id)
          if hasSamePoints(params[:giving], params[:receiving])
            params[:receiving].each do |item|
              updateSurvivorItems(@receiver_id, @giver_id, item)
            end

            params[:giving].each do |item|
              updateSurvivorItems(@giver_id, @receiver_id, item)
            end
          else
            raise ActiveRecord::Rollback, "Check total items points"
          end
        else
          raise ActiveRecord::Rollback, "Receiver is infected, cannot trade"
        end
      else
        raise ActiveRecord::Rollback, "Giver is infected, cannot trade"
      end
    end
  end

  def updateSurvivorItems(id1, id2, trade_item)
    item = SurvivorItem.find_by(survivor_id: id1, item_id: trade_item[:id])
    if item != nil
      item.quantity -= trade_item[:quantity]
      item.save
    else
      raise ActiveRecord::Rollback, "Survivor doesn't have items to trade"
    end

    item = SurvivorItem.find_by(survivor_id: id2, item_id: trade_item[:id])
    if item != nil
      item.quantity += trade_item[:quantity]
      item.save
    else
      SurvivorItem.create({
          survivor_id: id2,
          item_id: trade_item[:id],
          quantity: trade_item[:quantity],
          accessible: true
      })
    end
  end
end