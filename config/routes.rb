Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :survivors do
    member do
      put 'report_infection'
    end

    collection do
      put 'trade'
      get 'reports'
    end
  end
end
