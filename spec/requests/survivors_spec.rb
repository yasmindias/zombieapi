require 'rails_helper'

RSpec.describe "Survivors", type: :request do
  describe "GET /survivors" do
    it "returns all the survivors" do
      Survivor.create(name:"Yasmin", age:21, gender:"Female", last_location:"teste")
      Survivor.create(name:"Ludmilla", age:25, gender:"Male", last_location:"teste2")

      get "/survivors"
      expect(response).to have_http_status(200)

      json = JSON.parse(response.body)
      expect(json.length).to eq(2)

      expect(json[1]["name"]).to eq("Yasmin")
      expect(json[1]["gender"]).to eq("Female")
      expect(json[0]["name"]).to eq("Ludmilla")
    end
  end

  describe "GET /survivors/1" do
    it "returns one survivor" do
      survivor = Survivor.create(name:"Yasmin", age:21, gender:"Female", last_location:"teste")
      Survivor.create(name:"Ludmilla", age:25, gender:"Male", last_location:"teste2")

      get "/survivors/#{survivor.id}"

      expect(response).to have_http_status(200)

      json = JSON.parse(response.body)

      expect(json["name"]).to eq("Yasmin")
    end
  end
end

