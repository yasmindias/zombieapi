require 'rails_helper'

RSpec.describe TradeService, type: :request do
  describe "trade methods" do
    it "checks if survivor is infected" do
      Survivor.create(name:"Yasmin", age:21, gender:"Female", last_location:"teste", "reported_infection_count": 4)
      expect(TradeService.new().isInfected(1)).to eq(true)
    end

    it "checks if the item points are equal" do
      Item.create({name: "Water", points: 4})
      Item.create({name: "Food", points: 3})
      Item.create({name: "Medication", points: 2})
      Item.create({name: "Ammunition", points: 1})

      inventory1 = [{id: 1, quantity: 1}]
      inventory2 = [{id: 3, quantity: 2}]

      expect(TradeService.new().hasSamePoints(inventory1, inventory2)).to eq(true)
    end
  end
end
