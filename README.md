# Zombie Survival Social Network 

This is a technical test, a part of the process of the job application at Codeminer42.

## Installation

#### Requirements
* Rails 5.2
* Postgresql

#### Steps
1. Create database named 'zombieapi'
2. Clone repository
3. Run `bundle install`
4. Run `EDITOR=vim bin/rails credentials:edit` to edit the database credentials
5. Add 'dbusername' and 'dbpassword' key pair

##### Credentials format
```
# aws:
#   access_key_id: 123
#   secret_access_key: 345

# Used as the base secret for all MessageVerifiers in Rails, including the one protecting cookies.
secret_key_base: [HASH]

dbusername: [db_username]
dbpassword: [db_password]
```

6. Run `rails db:migrate` to create the schema
7. *Optional:* Run `rails db:seed` to populate the database

## Usage

### Register survivor

`POST /survivors`

**All fields are required.**

| Field         | Type       | Description         |
| ------------- |:----------:|:--------------------|
| name          | string     |Survivor's name      |
| age           | integer    |Survivor's age       |
| gender        | string     |Survivor's gender    |
| last_location | string     |Survivor's last location [latitude, longitude] |
| inventory     | array      |Survivor's inventory |
| id            | integer    |Id of the item |
| quantity      | integer    |Quantity of items |

#### JSON example
```json
{
  "name": "John Doe",
  "age": 25,
  "gender": "Male",
  "last_location": "40.7143528,-74.0059731",
  "inventory":[
    {
      "id":1,
      "quantity": 2
    }
  ]
}
```

### Update survivor's location

`PUT /survivors/:id`

| Field         | Type       | Description         |
| ------------- |:----------:|:--------------------|
| id            | integer    |Survivor's id        |
| last_location | string     |Survivor's last location [latitude, longitude] |

#### JSON example
```json
{
  "id": 20,
  "last_location": "40.7143528,-74.0059731",
}
```

### Report infection

`PUT /survivors/:id/report_infection`

The only information necessary is the survivor's id. After 3+ reports the survivor's inventory is made inaccessible.

### Trade items

Before the trade is initiated it is checked if both survivors are not infected and if they have the informed quantity of items.

**All fields are required**

`PUT /survivors/trade`

| Field          | Type       | Description         |
| -------------- |:----------:|:--------------------|
| receiver_id    | integer    |Id of the survivor who initiated the trade|
| giver_id       | integer    |Id of the survivor who was proposed the trade|
| receiving | array      |Items the receiver is willing to trade |
| id       | integer    |Id of the item |
| quantity | integer    |Quantity of items |
| giving    | array      |Items the giver was asked to trade |
| id       | integer    |Id of the item |
| quantity | integer    |Quantity of items |

#### JSON example
```json
{
  "giver_id": 20,
  "receiver_id": 20,
  "giving": [
    {
      "id": 1,
      "quantity": 1
    }
  ],
  "receiving": [
    {
      "id": 3,
      "quantity": 2
    }
  ]
}
```

### Reports

Returns the percentage of infected survivors, percentage of non-infected survivors, average amount of each kind of resource by survivor and total points lost because of infected survivor.

`GET /survivors/reports`

#### JSON example
```json
{
    "pct_infected_survivors": "14%",
    "pct_non_infected_survivors": "86%",
    "points_lost_due_to_infection": 61,
    "average_by_items": {
        "Water": 8,
        "Food": 7,
        "Medication": 10,
        "Ammunition": 4
    }
}
```
